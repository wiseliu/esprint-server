/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsk.esprint.model;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Version;
import org.hibernate.annotations.SQLDelete;

/**
 *
 * @author wisely
 */
@Entity
@Table(name = "profile", schema = "esprint")
@SQLDelete(sql = "UPDATE esprint.profiles SET profile_delete_timestamp = now() WHERE profile_id = ? AND profile_update_timestamp = ?")
public class Profile implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "profile_id")
    private long id;

    @Column(name = "profile_username")
    private String username;

    @Column(name = "profile_location", nullable = true)
    private String location;

    @Column(name = "profile_insert_timestamp", nullable = false, insertable = false, columnDefinition = "timestamp without time zone default now()")
    private Timestamp insertTimestamp;

    @Version
    @Column(name = "profile_update_timestamp", nullable = true)
    private Timestamp updateTimestamp;

    @Column(name = "profile_delete_timestamp", nullable = true)
    private Timestamp deleteTimestamp;

    @OneToOne
    @PrimaryKeyJoinColumn(name = "id_profile_id")
    @JoinColumn(name = "profile_id")
    private Credential credential;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Timestamp getInsertTimestamp() {
        return insertTimestamp;
    }

    public void setInsertTimestamp(Timestamp insertTimestamp) {
        this.insertTimestamp = insertTimestamp;
    }

    public Timestamp getUpdateTimestamp() {
        return updateTimestamp;
    }

    public void setUpdateTimestamp(Timestamp updateTimestamp) {
        this.updateTimestamp = updateTimestamp;
    }

    public Timestamp getDeleteTimestamp() {
        return deleteTimestamp;
    }

    public void setDeleteTimestamp(Timestamp deleteTimestamp) {
        this.deleteTimestamp = deleteTimestamp;
    }

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

}
