/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsk.esprint.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;
import org.hibernate.annotations.SQLDelete;

/**
 *
 * @author wisely
 */
@Entity
@Table(name = "persons", schema = "esprint")
@SQLDelete(sql = "UPDATE esprint.person SET person_delete_timestamp = now() WHERE person_id = ? AND person_update_timestamp = ?")
public class Person implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "person_id")
    private long id;

    @Column(name = "person_name")
    private String name;

    @Column(name = "person_position")
    private String position;

    @OneToMany(mappedBy = "person")
    @JoinColumn(name = "assignment_id")
    private List<Assignment> assignments;

    @Column(name = "person_insert_timestamp", nullable = false, insertable = false, columnDefinition = "timestamp without time zone default now()")
    private Timestamp insertTimestamp;

    @Version
    @Column(name = "person_update_timestamp", nullable = true)
    private Timestamp updateTimestamp;

    @Column(name = "person_delete_timestamp", nullable = true)
    private Timestamp deleteTimestamp;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Timestamp getInsertTimestamp() {
        return insertTimestamp;
    }

    public void setInsertTimestamp(Timestamp insertTimestamp) {
        this.insertTimestamp = insertTimestamp;
    }

    public Timestamp getUpdateTimestamp() {
        return updateTimestamp;
    }

    public void setUpdateTimestamp(Timestamp updateTimestamp) {
        this.updateTimestamp = updateTimestamp;
    }

    public Timestamp getDeleteTimestamp() {
        return deleteTimestamp;
    }

    public void setDeleteTimestamp(Timestamp deleteTimestamp) {
        this.deleteTimestamp = deleteTimestamp;
    }

    public List<Assignment> getAssignments() {
        return assignments;
    }

    public void setAssignments(List<Assignment> assignments) {
        this.assignments = assignments;
    }

}
