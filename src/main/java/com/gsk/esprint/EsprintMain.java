package com.gsk.esprint;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EsprintMain {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        SpringApplication.run(EsprintMain.class, args);
    }
}
