/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * And open the template in the editor.
 */
package com.gsk.esprint.repository;

import com.gsk.esprint.model.Event;
import java.sql.Timestamp;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 *
 * @author wisely
 */
public interface EventRepository extends SoftDeleteRepository<Event, Long> {

    public Page<Event> findByNameLikeAndDeleteTimestampIsNull(String name_part, Pageable page);

    public Event findOneByNameAndDeleteTimestampIsNull(String name);

    public Page<Event> findByStartTimestampGreaterThanEqualAndDeleteTimestampIsNull(Timestamp time, Pageable page);

    public Page<Event> findByEndTimestampGreaterThanEqualAndDeleteTimestampIsNull(Timestamp time, Pageable page);

    public Page<Event> findByStartTimestampLessThanEqualAndDeleteTimestampIsNull(Timestamp time, Pageable page);

    public Page<Event> findByEndTimestampLessThanEqualAndDeleteTimestampIsNull(Timestamp time, Pageable page);

    public Page<Event> findByStartTimestampBetweenAndDeleteTimestampIsNull(Timestamp time1, Timestamp time2, Pageable page);

    public Page<Event> findByEndTimestampBetweenAndDeleteTimestampIsNull(Timestamp time1, Timestamp time2, Pageable page);

}
