/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsk.esprint.repository;

import com.gsk.esprint.model.Credential;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author wisely
 */
public interface CredentialRepository extends JpaRepository<Credential, Long>{

}
