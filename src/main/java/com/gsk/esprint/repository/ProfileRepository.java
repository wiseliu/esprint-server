/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsk.esprint.repository;

import com.gsk.esprint.model.Profile;

/**
 *
 * @author wisely
 */
public interface ProfileRepository extends SoftDeleteRepository<Profile, Long> {
    
}
