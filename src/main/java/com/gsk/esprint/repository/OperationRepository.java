/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsk.esprint.repository;

import com.gsk.esprint.model.Event;
import com.gsk.esprint.model.Operation;
import com.gsk.esprint.model.Profile;
import java.sql.Timestamp;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 *
 * @author wisely
 */
public interface OperationRepository extends SoftDeleteRepository<Operation, Long> {

    public Page<Operation> findByNameLikeAndDeleteTimestampIsNull(String name_part, Pageable page);

    public Operation findOneByNameAndDeleteTimestampIsNull(String name);

    public Page<Operation> findByLocationLikeAndDeleteTimestampIsNull(String location_part, Pageable page);

    public Operation findOneByLocationAndDeleteTimestampIsNull(String location);

    public Page<Operation> findByStartTimestampGreaterThanEqualAndDeleteTimestampIsNull(Timestamp time, Pageable page);

    public Page<Operation> findByEndTimestampGreaterThanEqualAndDeleteTimestampIsNull(Timestamp time, Pageable page);

    public Page<Operation> findByStartTimestampLessThanEqualAndDeleteTimestampIsNull(Timestamp time, Pageable page);

    public Page<Operation> findByEndTimestampLessThanEqualAndDeleteTimestampIsNull(Timestamp time, Pageable page);

    public Page<Operation> findByStartTimestampBetweenAndDeleteTimestampIsNull(Timestamp time1, Timestamp time2, Pageable page);

    public Page<Operation> findByEndTimestampBetweenAndDeleteTimestampIsNull(Timestamp time1, Timestamp time2, Pageable page);

    public Page<Operation> findByPolsekAndDeleteTimestampIsNull(Profile polsek, Pageable page);

    public Page<Operation> findByEventAndDeleteTimestampIsNull(Event event, Pageable page);

}
