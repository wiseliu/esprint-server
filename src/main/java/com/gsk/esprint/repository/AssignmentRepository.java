/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsk.esprint.repository;
import com.gsk.esprint.model.Assignment;
import com.gsk.esprint.model.Operation;
import com.gsk.esprint.model.Person;

/**
 *
 * @author wisely
 */
public interface AssignmentRepository extends SoftDeleteRepository<Assignment, Long> {
    
    public Assignment findOneByPersonAndOperationAndDeleteTimestampIsNull(Person person, Operation operation);
    

}
