/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * And open the template in the editor.
 */
package com.gsk.esprint.repository;

import java.io.Serializable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

/**
 *
 * @author wisely
 * @param <T>
 * @param <ID>
 */
@NoRepositoryBean
public interface SoftDeleteRepository<T, ID extends Serializable> extends JpaRepository<T, ID> {

    public T findOneByIdAndDeleteTimestampNotNull(ID id);

    public T findOneByIdAndDeleteTimestampIsNull(ID id);

    public Page<T> findAllByIdAndDeleteTimestampIsNotNull(Pageable page);

    public Page<T> findAllByIdAndDeleteTimestampIsNull(Pageable page);

}
