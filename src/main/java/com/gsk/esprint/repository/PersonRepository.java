/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsk.esprint.repository;

import com.gsk.esprint.model.Person;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 *
 * @author wisely
 */
public interface PersonRepository extends SoftDeleteRepository<Person, Long> {

    public Person findOneByNameAndDeleteTimestampIsNull(String name);

    public Page<Person> findByNameLikeAndDeleteTimestampIsNull(String name_part, Pageable page);

    public Person findOneByPositionAndDeleteTimestampIsNull(String position);

    public Page<Person> findByPositionLikeAndDeleteTimestampIsNull(String position_part, Pageable page);

}
